    const TopBoxDiv = document.getElementById('TopBox');
    const SearchInputBoxSection = document.getElementById('SearchInputBox');
    const infoTxtp = document.getElementById('infoTxt');
    const SearchDivDiv = document.getElementById('SearchDiv');
    const WeatherDetailsDiv = document.getElementById('WeatherDetails');
    const imgImage = document.getElementById('img');
    const cooldataDiv = document.getElementById('cooldata');
    const ArrowI = document.getElementById('Arrow');
    const bottomDetailsDiv = document.getElementById('bottomDetails');
    const currentBtnButton = document.getElementById('currentBtn');
    const NumberSpan = document.querySelector('.Number');
    const SearchDivInput = document.querySelector('#SearchDiv input');
    const Arrowi = document.getElementById('Arrow');
    const weatherDiv = document.querySelector('.weather');

    function getWeather(cityName, countryCode) {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${cityName},${countryCode}&appid=beb759f15252cf39668e78190192d049&units=metric`)
            .then((response) => response.json())
            .then((data) => {
                const countryCode = data.sys.country;

                // Below API for Convert country code to country full name

                fetch(`https://restcountries.com/v2/alpha/${countryCode}`)
                    .then(response => response.json())
                    .then(countryData => {
                        const countryName = countryData.name;
                        NumberSpan.innerHTML = Math.round(data.main.temp);
                        cooldata.querySelector(".location span").innerText = `${cityName}, ${countryName}`;
                        document.querySelector(".numb2").innerHTML = data.wind.speed + " km/h";
                        document.querySelector(".Humi").innerHTML = data.main.humidity + "%"
                        document.querySelector(".weather").innerHTML = data.weather[0].main.toLowerCase();

                        const weatherId = data.weather[0].id;
                        const iconPath = getImagePath(weatherId);
                        imgImage.src = iconPath; 

                        // Additional code to display other weather details if needed
                        SearchDivDiv.style.display = 'none';
                        WeatherDetailsDiv.style.display = 'block';
                        Arrowi.style.display = 'block';
                        infoTxtp.innerHTML = "";

                    })
                    .catch(error => {
                        console.error('Error fetching country data:', error);
                    });
            })
            .catch((error) => {
                SearchDivDiv.style.display = 'block';
                WeatherDetailsDiv.style.display = 'none';

                infoTxtp.classList.add("error");
                infoTxtp.innerHTML = "Type a valid city name 😕";
            });
    }

    SearchDivInput.addEventListener("keyup", e => {
        // if user pressed enter btn and input value is not empty
        if (e.key === "Enter" && SearchDivInput.value.trim() !== "") {
            const cityName = SearchDivInput.value.trim();
            infoTxtp.innerHTML = "";
            infoTxtp.classList.remove("error");
            getWeather(cityName);
        }
        // if user type wrong city name or empty value.
        else {
            infoTxtp.innerHTML = "Type a city name and Enter 😊";
            infoTxtp.classList.remove("error");
            SearchDivDiv.style.display = 'block';
            WeatherDetailsDiv.style.display = 'none';
        }
    });

    // if user wants return to main page and wants to search new city.
    ArrowI.addEventListener("click", (even) => {
        SearchDivDiv.style.display = 'block';
        WeatherDetailsDiv.style.display = 'none';
        SearchDivInput.value = "";
        Arrowi.style.display = 'none';
    })

    //  Below code for assigning specific images for weather accordingly

    function getImagePath(weatherId) {
        switch (weatherId) {
            case 200:
            case 201:
            case 202:
            case 210:
            case 211:
            case 212:
            case 221:
            case 230:
            case 231:
            case 232:
                return 'icons/storm.svg';
            case 300:
            case 301:
            case 302:
            case 310:
            case 311:
            case 312:
            case 313:
            case 314:
            case 321:
                return 'icons/haze.svg';
            case 500:
            case 501:
            case 502:
            case 503:
            case 504:
                return 'icons/rain.svg';
            case 511:
            case 520:
            case 521:
            case 522:
            case 531:
                return 'icons/snow.svg';
            case 600:
            case 601:
            case 602:
            case 611:
            case 612:
            case 613:
            case 615:
            case 616:
            case 620:
            case 621:
            case 622:
                return 'icons/snow.svg';
            case 701:
            case 711:
            case 721:
            case 731:
            case 741:
            case 751:
            case 761:
            case 762:
            case 771:
            case 781:
                return 'icons/smoke.png';
            case 800:
                return 'icons/clear.svg';
            case 801:
            case 802:
            case 803:
            case 804:
                return 'icons/cloud.svg';
            default:
                return 'icons/clear.svg'; // Default icon
        }
    }

    // below code for to check user device location temperature

    function getCurrentLocationWeather() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;
                fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=beb759f15252cf39668e78190192d049&units=metric`)
                    .then((response) => response.json())
                    .then((data) => {
                        const cityName = data.name;
                        const countryCode = data.sys.country;
                        getWeather(cityName, countryCode);
                    })
                    .catch((error) => {
                        console.error('Error fetching weather data:', error);
                    });
            });
        } else {
            console.error("Geolocation is not supported by this browser.");
        }
    }

    currentBtnButton.addEventListener("click", getCurrentLocationWeather);
